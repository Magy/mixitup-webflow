var conv = function (str) {
    if (!str) {
        str = 'empty';
    }
    return str.replace(/[!\"#$%&'\(\)\*\+,\.\/:;<=>\?\@\[\\\]\^`\{\|\}~]/g, '')
        .replace(/ /g, "-")
        .toLowerCase()
        .trim();
};

var filterGroups = document.querySelectorAll('.filter-group');
filterGroups.forEach( function(group) {
    group.setAttribute('data-filter-group','');
});

var mixer = mixitup(document.querySelector('.container'), {
    multifilter: {
        enable: true                 
    }
});
